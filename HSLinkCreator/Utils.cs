﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSLinkCreator
{
    public sealed class Utils
    {
        public static string GetFileName(string Path)
        {
            int index = Path.LastIndexOf("\\");
            return Path.Substring(index + 1);
        }
        public static string GetDirectory(string Path)
        {
            int index = Path.LastIndexOf("\\");
            return Path.Remove(index);
        }
        public static string GetDrive(string Path)
        {
            int index = Path.IndexOf("\\");
            return Path.Remove(index);
        }
    }
}
