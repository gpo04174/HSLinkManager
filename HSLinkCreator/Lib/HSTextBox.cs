﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace HSUtils
{
    public class HSTextBox : TextBox
    {
        public bool AutoScroll { get; set; }
        public new void AppendText(string Text){ AppendTextToTextBox(Text, AutoScroll);}
        public new string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                if (AutoScroll)
                {
                    SelectionStart = Text.Length;
                    ScrollToCaret();
                    Refresh();
                }
            }
        }

        // Constants for extern calls to various scrollbar functions
        private const int SB_VERT = 0x1;
        private const int WM_VSCROLL = 0x115;
        private const int SB_THUMBPOSITION = 0x4;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int GetScrollPos(IntPtr hWnd, int nBar);
        [DllImport("user32.dll")]
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);
        [DllImport("user32.dll")]
        private static extern bool PostMessageA(IntPtr hWnd, int nBar, int wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern bool GetScrollRange(IntPtr hWnd, int nBar, out int lpMinPos, out int lpMaxPos);
        private void AppendTextToTextBox(string text, bool autoscroll)
        {
            base.AppendText(text);
            /*
            int savedVpos = GetScrollPos(Handle, SB_VERT);
            if (autoscroll)
            {
                base.AppendText(text);
                int VSmin, VSmax;
                GetScrollRange(Handle, SB_VERT, out VSmin, out VSmax);
                int sbOffset = (int)((ClientSize.Height - SystemInformation.HorizontalScrollBarHeight) / (base.Font.Height));
                savedVpos = VSmax - sbOffset;
            }
            //SetScrollPos(Handle, SB_VERT, savedVpos, true);
            PostMessageA(Handle, WM_VSCROLL, SB_THUMBPOSITION + 0x10000 * savedVpos, 0);
            */
        }
    }
}
