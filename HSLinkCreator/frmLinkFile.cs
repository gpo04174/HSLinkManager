﻿//using DaveChambers.FolderBrowserDialogEx;
using HSLinkCreator.Core;
using HSLinkCreator.Language;
using System;
using System.Windows.Forms;

namespace HSLinkCreator
{
    public partial class frmLinkFile : Form, IUpdateLanguage
    {
        public frmLinkFile()
        {
            InitializeComponent();
            LanguageManager.LanguageChanged += (s, e) => UpdateLanguage();
        }

        public new DialogResult ShowDialog() { DialogResult = DialogResult.None; base.ShowDialog(); return DialogResult; }
        public LinkFile[] Links { get {return linkFileControl1.Links;} }

        private void frmFileLink_Load(object sender, EventArgs e)
        {
            this.FormClosing += (o, e1) => { if (DialogResult == DialogResult.None) DialogResult = DialogResult.Cancel; };

            UpdateLanguage();
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }

        public void UpdateLanguage()
        {
            Text = LanguageManager.Language["STR_FRM_LINKFILE_TITLE"];
            btnOK.Text = LanguageManager.Language["STR_BTN_OK"];
        }
    }
}
