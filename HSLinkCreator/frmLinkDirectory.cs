﻿//using DaveChambers.FolderBrowserDialogEx;
using HSLinkCreator.Core;
using HSLinkCreator.Language;
using System;
using System.Windows.Forms;

namespace HSLinkCreator
{
    public partial class frmLinkDirectory : Form
    {
        public frmLinkDirectory()
        {
            InitializeComponent();
        }
        public new DialogResult ShowDialog() { DialogResult = DialogResult.None; base.ShowDialog(); return DialogResult; }

        public LinkDirectory[] Links { get { return null; } }

        private void frmLinkDirectory_Load(object sender, EventArgs e)
        {
            this.FormClosing += (o, e1) => { if (DialogResult == DialogResult.None) DialogResult = DialogResult.Cancel; };

            UpadateLanguage();
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }


        void UpadateLanguage()
        {
            this.Text = LanguageManager.Language["STR_FRM_LINKDIR_TITLE"];
            btnOK.Text = LanguageManager.Language["STR_BTN_OK"];
        }
    }
}
