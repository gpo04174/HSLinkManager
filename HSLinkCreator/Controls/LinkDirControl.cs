﻿using HSLinkCreator.Core;
using HSLinkCreator.Language;
using Ionic.Utils;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HSLinkCreator.Controls
{
    public partial class LinkDirControl : UserControl
    {
        public LinkDirControl()
        {
            InitializeComponent();
        }

        public LinkDirectory[] Links
        {
            get
            {
                LinkDirectory[] Links = new LinkDirectory[listView1.Items.Count];
                for (int i = 0; i < Links.Length; i++)
                    Links[i] = listView1.Items[i].Tag as LinkDirectory;
                return Links;
            }
        }
        private void LinkDirControl_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 1;
            listView1.ApplyTheme = true;

            UpadateLanguage();
        }
        
        private void btnDestination_Click(object sender, EventArgs e)
        {
            FolderBrowserDialogEx dialog = FolderBrowserDialogEx.LocalBrowser(LanguageManager.Language["STR_FRM_LINKDIR_DLG_ORIG"]);
            dialog.SelectedPath = textBox1.Text;
            //dialog.RootFolder = Environment.SpecialFolder.Desktop;
            if (dialog.ShowDialog(this) == DialogResult.OK)
                textBox1.Text = dialog.SelectedPath;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show(LanguageManager.Language["STR_FRM_LINKDIR_DLG_ORIG"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnDestination_Click(sender, e);
            }
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                FolderBrowserDialogEx dialog1 = FolderBrowserDialogEx.LocalBrowser(LanguageManager.Language["STR_FRM_LINKDIR_DLG_DEST"]);
                dialog1.NewStyle = true;
                if (dialog1.ShowDialog(this) == DialogResult.OK) await Add(false, dialog1.SelectedPath);
            }
        }
        private void btnClear_Click(object sender, EventArgs e) { if (MessageBox.Show(LanguageManager.Language["STR_FRM_LINKDIR_MSG_CLERALL"], Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.OK) listView1.Items.Clear(); }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) deleteToolStripMenuItem_Click(sender, e);
        }

        int SetLinkKind(LinkKind kind)
        {
            switch (kind)
            {
                case LinkKind.Shortcut: return 0;
                case LinkKind.Special: return 2;
                default: return 1;
            }
        }
        LinkKind GetLinkKind()
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0: return LinkKind.Shortcut;
                case 2: return LinkKind.Special;
                default: return LinkKind.SymLink;
            }
        }

        private void chkFileOnly_CheckedChanged(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            comboBox1.Items.Clear();
            if (chkFileOnly.Checked)
            {
                comboBox1.Items.AddRange(new string[] {
                    LanguageManager.Language["STR_SHORTCUT"],
                    LanguageManager.Language["STR_SYMLINK"],
                    LanguageManager.Language["STR_HARDLINK"]});
            }
            else
            {
                comboBox1.Items.AddRange(new string[] {
                    LanguageManager.Language["STR_SHORTCUT"],
                    LanguageManager.Language["STR_SYMLINK"],
                    LanguageManager.Language["STR_JUNCLINK"] });
            }
            comboBox1.SelectedIndex = index < 0 ? 1 : index;
        }


        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            /*
            btnEdit.Enabled = listView1.SelectedItems.Count == 1;
            if (listView1.SelectedItems.Count == 1)
            {
                LinkDirectory sel = dirs[listView1.SelectedItems[0].Index] as LinkDirectory;

                //int j = 0;
                //for (int i = 0; i < dir_sel.DestinationPath.Length && j < 2; i++)
                //    if (dir_sel.DestinationPath[i] == '\\') j++;
                if (sel != null)
                {
                    textBox1.Text = sel.DestinationPath.Remove(sel.DestinationPath.LastIndexOf("\\") + 1);
                    chkFileOnly.Checked = false;
                    comboBox1.SelectedIndex = SetLinkKind(sel.Kind);
                }
                else if(file_sel != null)
                {
                    chkFileOnly.Checked = true;
                    comboBox1.SelectedIndex = SetLinkKind(file_sel.Kind);
                }
            }
            */
        }


        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LinkDirectory[] linkd = new LinkDirectory[listView1.SelectedItems.Count];
            ListViewItem[] itemd = new ListViewItem[linkd.Length];
            for (int i = 0; i < linkd.Length; i++)
            {
                itemd[i] = listView1.SelectedItems[i];
                linkd[i] = itemd[i].Tag as LinkDirectory;
            }

            frmEdit rename = new frmEdit(linkd);
            if (rename.ShowDialog() == DialogResult.OK)
                for (int i = 0; i < itemd.Length; i++) Edit(itemd[i], rename.Link[i] as LinkDirectory);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = listView1.SelectedItems.Count - 1; i >= 0; i--)
                listView1.SelectedItems[i].Remove();
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            deleteToolStripMenuItem.Enabled = listView1.SelectedItems.Count > 0;
            editToolStripMenuItem.Enabled = listView1.SelectedItems.Count == 1;
        }

        #region Core
        bool ShowMessaged = false;
        LinkProgressAdd InitProgress(int TotalCount = 0)
        {
            LinkProgressAdd p = new LinkProgressAdd();
            p.DoneClicked += (e) => panel1.Enabled = true;
            p.Size = new System.Drawing.Size(350, 100);
            int x = (Width - p.Width) / 2;
            int y = (Height - p.Height) / 2;
            p.Location = new System.Drawing.Point(x, y);
            this.SizeChanged += (o, e) =>
            {
                if (!p.IsDisposed)
                {
                    x = (Width - p.Width) / 2;
                    y = (Height - p.Height) / 2;
                    p.Location = new System.Drawing.Point(x, y);
                }
            };
            p.TotalCount = TotalCount;
            p.BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(p);
            p.BringToFront();
            return p;
        }
        ListViewItem Make(string Dir, LinkKind kind, bool ShowMessage = true)
        {
            string Orig = Dir.Remove(Dir.IndexOf("\\"));
            string Dest = textBox1.Text.Remove(textBox1.Text.IndexOf("\\"));

            if (!chkFileOnly.Checked && kind == LinkKind.Special && Orig.ToUpper() != Dest.ToUpper()) kind = LinkKind.SymLink;

            LinkDirectory link = null;
            string path =
                textBox1.Text +
                (textBox1.Text[textBox1.Text.Length - 1] == '\\' ? "" : "\\") +
                Path.GetFileName(Dir);
            if (chkFileOnly.Checked) link = new LinkDirectory(Dir, path, kind, chkRecursive.Checked);
            else link = new LinkDirectory(Dir, path, kind);

            string dest = link.DestinationPath + (link.Kind == LinkKind.Shortcut ? ".lnk" : null);
            ListViewItem li = new ListViewItem(Path.GetFileName(dest[dest.Length - 1] == '\\' ? dest.Remove(dest.Length - 1) : dest));

            if (link.Kind == LinkKind.Shortcut) li.SubItems.Add(LanguageManager.Language["STR_SHORTCUT"]);
            else if (link.Kind == LinkKind.SymLink) li.SubItems.Add(LanguageManager.Language["STR_SYMLINK"]);
            else
            {
                if (link.IsOnlyFile) li.SubItems.Add(LanguageManager.Language["STR_HARDLINK"]);
                else li.SubItems.Add(LanguageManager.Language["STR_JUNCLINK"]);
            }

            li.SubItems.Add(link.IsOnlyFile ? (link.Recursive ? "●" : "○") : "");
            li.SubItems.Add(dest);
            li.SubItems.Add(link.OriginalPath);
            li.Tag = link;
            return li;
        }
        ListViewItem[] Makes(params string[] Dirs)
        {
            ListViewItem[] items = new ListViewItem[Dirs.Length];
            LinkKind kind = GetLinkKind();
            for (int i = 0; i < Dirs.Length; i++) items[i] = Make(Dirs[i], kind);
            return items;
        }
        async Task Add(bool UseBuffer, params string[] Dirs)
        {
            int cnt = UseBuffer ? Program.BufferCnt : 1;
            int j = 1;
            ListViewItem[] items = new ListViewItem[cnt];

            panel1.Enabled = false;
            LinkProgressAdd p = InitProgress(Dirs.Length);
            try
            {
                LinkKind kind = GetLinkKind();
                for (int i = 0; i < Dirs.Length && !p.IsCancel; i++)
                {
                    p.CurrentCount++;
                    try
                    {
                        items[j - 1] = Make(Dirs[i], kind);
                        p.Added++;
                        if (j == items.Length)
                        {
                            j = 0;
                            listView1.Items.AddRange(items);
                            await Task.Delay(1);
                        }
                        j++;
                    }
                    catch { p.Fail++; }
                    p.Update();
                }
                for (int i = 0; i < j - 1; i++)
                {
                    listView1.Items.Add(items[i]);
                    await Task.Delay(1);
                }
            }
            finally { p.Done(true);}
        }
        void Edit(int index, LinkDirectory link) { Edit(listView1.Items[index], link); }
        void Edit(ListViewItem li, LinkDirectory link)
        {
            li.Tag = link;
            li.SubItems[0].Text = Path.GetFileName(link.DestinationPath);
            li.SubItems[2].Text = link.IsOnlyFile ? (link.Recursive ? "●" : "○") : "";
            li.SubItems[3].Text = link.DestinationPath;
            li.SubItems[4].Text = link.OriginalPath;

            if (link.Kind == LinkKind.Shortcut) li.SubItems[1].Text = LanguageManager.Language["STR_SHORTCUT"];
            else if (link.Kind == LinkKind.SymLink) li.SubItems[1].Text = LanguageManager.Language["STR_SYMLINK"];
            else
            {
                if (link.IsOnlyFile) li.SubItems[1].Text = LanguageManager.Language["STR_HARDLINK"];
                else li.SubItems[1].Text = LanguageManager.Language["STR_JUNCLINK"];
            }
        }
        void UpadateLanguage()
        {
            lblPath.Text = LanguageManager.Language["STR_FRM_LBL_PATH"];
            lblType.Text = LanguageManager.Language["STR_FRM_LBL_TYPE"];
            btnAdd.Text = LanguageManager.Language["STR_FRM_BTN_ADD"];
            chkFileOnly.Text = LanguageManager.Language["STR_FRM_LINKDIR_CHK_OF"];
            chkRecursive.Text = LanguageManager.Language["STR_FRM_LINKDIR_CHK_RECUR"];
            btnClear.Text = LanguageManager.Language["STR_FRM_BTN_CLRALL"];
            listView1.Columns[0].Text = LanguageManager.Language["STR_FRM_TAB_NAME"];
            listView1.Columns[1].Text = LanguageManager.Language["STR_FRM_TAB_NAME"];
            listView1.Columns[2].Text = LanguageManager.Language["STR_FRM_TAB_OF"];
            listView1.Columns[3].Text = LanguageManager.Language["STR_FRM_TAB_DEST"];
            listView1.Columns[4].Text = LanguageManager.Language["STR_FRM_TAB_ORIG"];
            comboBox1.Items[0] = LanguageManager.Language["STR_SHORTCUT"];
            comboBox1.Items[1] = LanguageManager.Language["STR_SYMLINK"];
            comboBox1.Items[2] = LanguageManager.Language["STR_JUNCLINK"];
            editToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_EDIT"];
            deleteToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_DELETE"];

            for (int i = 0; i < listView1.Items.Count; i++)
            {
                LinkDirectory link = listView1.Items[i].Tag as LinkDirectory;
                if (link.Kind == LinkKind.Shortcut) listView1.Items[i].SubItems[1].Text = LanguageManager.Language["STR_SHORTCUT"];
                else if (link.Kind == LinkKind.SymLink) listView1.Items[i].SubItems[1].Text = LanguageManager.Language["STR_SYMLINK"];
                else
                {
                    if (link.IsOnlyFile) listView1.Items[i].SubItems[1].Text = LanguageManager.Language["STR_HARDLINK"];
                    else listView1.Items[i].SubItems[1].Text = LanguageManager.Language["STR_JUNCLINK"];
                }
            }
        }
        #endregion

        private async void listView1_DragDrop(object sender, DragEventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show(LanguageManager.Language["STR_FRM_LINKDIR_DLG_ORIG"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnDestination_Click(sender, e);
            }
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                panel1.Enabled = false;
                LinkProgressAdd p = InitProgress();
                LinkKind kind = GetLinkKind();
                try
                {
                    string[] dirs = (string[])e.Data.GetData(DataFormats.FileDrop);
                    p.TotalCount = dirs.Length;
                    //p.Start();

                    ListViewItem[] buffer = new ListViewItem[Program.BufferCnt];
                    int j = 1;
                    for (int i = 0; i < dirs.Length; i++)
                    {
                        p.CurrentCount++;
                        try
                        {
                            if (Directory.Exists(dirs[i]))
                            {
                                buffer[j - 1] = Make(dirs[i], kind);
                                p.Added++;

                                if (j == buffer.Length)
                                {
                                    j = 0;
                                    listView1.Items.AddRange(buffer);
                                    await Task.Delay(1);
                                }
                                j++;
                            }
                            else p.Pass++;
                        }
                        catch(Exception ex) { p.Fail++; }
                        p.Update();
                    }
                    for (int i = 0; i < j - 1; i++)
                    {
                        listView1.Items.Add(buffer[i]);
                        await Task.Delay(1);
                    }
                }
                finally { p.Done(false); }//Controls.Remove(p); p.Dispose();
            }
            else MessageBox.Show(LanguageManager.Language["STR_FRM_LINKDIR_DLG_ORIG"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex == 2 && !ShowMessaged)
            {
                MessageBox.Show(LanguageManager.Language["STR_MSG_HARDLINK_WARN"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ShowMessaged = true;
            }
        }
    }
}
