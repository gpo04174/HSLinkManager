﻿using System;
using System.Diagnostics;
using System.Threading;

namespace HSLinkCreator.Core
{
    //ExitCode
    public enum Result : int { Starting = -2, Unknown = -1, Success = 0, Fail = 1 }
    public enum DoneResult { Complete, Cancel, Cancel_Fail }
    public enum ProgressKind { Start, Create, Delete, Attribute, Time_Create, Time_Access, Time_LastWrite }//, Time_Modify

    /// <summary>
    /// 익명함수를 위한 델리게이트
    /// </summary>
    public delegate void Action();
    public delegate void Action<T>(T args);
    public delegate void ProgressingEventHandler(object sender, Link link);
    public delegate void ProgressedEventHandler(object sender, ProgressEventArgs e);
    public delegate void LoggingEventHandler(object sender, LoggingEventArgs e);
    public delegate void CompleteEventHandler(object sender, DoneResult result);
    public abstract class MakeLink : IDisposable
    {
        protected System.Windows.Forms.Control UIControl;
        protected Thread thread;
        protected ParameterizedThreadStart start;

        public event ProgressingEventHandler Progressing;
        public event ProgressedEventHandler Progressed;
        public event CompleteEventHandler Complete;
        public event LoggingEventHandler Logging;

        public bool ApplyProperty { get; set; }
        public bool ApplyDate { get; set; }
        public bool LinkReplace { get; set; }

        public int TotalCount { get; protected set; }
        public int CurrentCount { get; protected set; }
        public Link Current { get; protected set; }
        public float Percent { get {return (CurrentCount / (float)TotalCount) * 100; } }

        public virtual void Dispose()
        {
            try { if (thread != null) thread.Abort(); } catch { }
        }

        //public abstract ProgressEventArgs[] Start(params ILink[] Job);
        public abstract void Start(params Link[] Job);
        public abstract void Stop();

        protected void OnProgressing(object sender, Link link)
        {
            try
            {
                if (UIControl != null && UIControl.InvokeRequired)
                    UIControl.Invoke(new Action<Link>((Link e) => Progressing?.Invoke (this, e)), link);
                else Progressing?.Invoke (this, link);
            }
            catch (Exception ex) { Trace.WriteLine(ex.ToString()); }
        }
        protected void OnProgressed(object sender, ProgressEventArgs e)
        {
            try
            {
                if (UIControl != null && UIControl.InvokeRequired)
                    UIControl.Invoke(new Action<ProgressEventArgs>((ProgressEventArgs e1) => Progressed?.Invoke(this, e1)), e);
                else Progressed?.Invoke(this, e);
            }
            catch (Exception ex) { Trace.WriteLine(ex.ToString()); }
        }
        protected void OnComplete(object sender, DoneResult result)
        {
            try
            {
                if (UIControl != null && UIControl.InvokeRequired)
                    UIControl.Invoke(new Action<DoneResult>((DoneResult e) => Complete?.Invoke(this, e)), result);
                else Complete?.Invoke(this, result);
            }
            catch (Exception ex) { Trace.WriteLine(ex.ToString()); }
        }
        protected void OnLogging(object sender, LoggingEventArgs e)
        {
            try
            {
                if (UIControl != null && UIControl.InvokeRequired)
                    UIControl.Invoke(new Action<LoggingEventArgs>((LoggingEventArgs e1) => Logging?.Invoke(this, e1)), e);
                else Logging?.Invoke(this, e);
            }
            catch (Exception ex) { Trace.WriteLine(ex.ToString()); }
        }


        #region Core
        public static string NowTime() { DateTime dt = DateTime.Now; return string.Format("{0:D2}:{1:D2}:{2:D2}.{3:D3}", dt.Hour, dt.Minute, dt.Second, dt.Millisecond); }
        /*
        public void SendLogging(string Message)
        {
            try
            {
                if (UIThreadsafe != null && UIThreadsafe.InvokeRequired)
                    UIThreadsafe.Invoke(new Action(() => Logging.Invoke(this, Message)));
                else Logging.Invoke(this, Message);
            }
            catch { }
        }
        */

        //public static string
        #endregion

    }
}