﻿using System;

namespace HSLinkCreator.Core
{
    public enum LinkKind
    {
        /// <summary>
        /// Create Shortcut (Commmon)
        /// </summary>
        Shortcut,
        /// <summary>
        /// Create SymLink (Commmon)
        /// </summary>
        SymLink,
        /// <summary>
        /// Special Link (FIle: HardLink, Directory: Junction Link)
        /// </summary>
        Special,
    }
    /*
        /// <summary>
        /// Custom Mode (Directory Only, Reserved)
        /// </summary>
        HardLink,
        /// <summary>
        /// Create Directory Junction (Directory Only)
        /// </summary>
        Junction,
        /// <summary>
        /// Custom Mode (Directory Only, Reserved)
        /// </summary>
        Custom,
        /// <summary>
        /// Special Link (FIle: HardLink, Directory: Junction Link)
        /// </summary>
        Special
        /// <summary>
        /// Create Directory and make SymLink (Directory Only)
        /// </summary>
        Special_SymLink,
        /// <summary>
        /// Create Directory and make HardLink (Directory Only)
        /// </summary>
        Special_HardLink,
     */

    public abstract class Link
    {
        static Random random = new Random(20190404);
        static uint Generate() { return (uint)random.Next(int.MinValue, int.MaxValue); }

        internal Link(string OriginalPath, string DestinationPath, LinkKind Kind)
        {
            this.OriginalPath = OriginalPath;
            this.DestinationPath = DestinationPath;
            this.Kind = Kind;
            ID = Generate();
        }
        public uint ID { get; private set; }

        public string OriginalPath { get; set; }
        public string DestinationPath { get; set; }
        public LinkKind Kind { get; set; }
        public string GetKindString() { return Kind.ToString(); }
    }
    public class LinkFile : Link
    {
        public LinkFile(string OriginalPath, string DestinationPath, LinkKind Kind) :
            base(OriginalPath, DestinationPath, Kind){ }
    }
    public class LinkDirectory : Link
    {
        public LinkDirectory(string OriginalPath, string DestinationPath, LinkKind Kind) :
            base(OriginalPath, DestinationPath, Kind){ }
        public LinkDirectory(string OriginalPath, string DestinationPath,
            LinkKind FileKind, bool Recursive) :
            base(OriginalPath, DestinationPath, FileKind)
        { IsOnlyFile = true; this.Recursive = Recursive; }

        public bool IsOnlyFile { get; set; }
        public bool Recursive { get; set; }
    }
}