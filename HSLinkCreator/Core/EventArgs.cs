﻿using HSLinkCreator.Language;

namespace HSLinkCreator.Core
{
    public class ProgressEventArgs
    {
        public ProgressEventArgs(Link Link, Result Result, string Message = null)
        { this.Link = Link; this.Result = Result; this.Message = Message; }
        public Link Link { get; protected set; }
        public Result Result { get; protected set; }

        public string Message { get; private set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
    public class LoggingEventArgs : ProgressEventArgs
    {
        public LoggingEventArgs(ProgressKind Kind, ProgressEventArgs e) :
            base(e.Link, e.Result, e.Message)
        { this.Kind = Kind; }
        public LoggingEventArgs(Link Link, ProgressKind Kind, Result Result, string Message = null) :
            base(Link, Result, Message)  { this.Kind = Kind; }
        public ProgressKind Kind { get; private set; }

        public override string ToString()
        {
            //return LanguageManager.Language[""];
            switch (Kind)
            {
                case ProgressKind.Start: return LanguageManager.Language["STR_CORE_MSG_LINK_START"];
                case ProgressKind.Create:
                    if (Result == Result.Success) return LanguageManager.Language["STR_CORE_MSG_LINK_OK"];
                    else if(Result == Result.Fail) return LanguageManager.Language["STR_CORE_MSG_LINK_FAIL"];
                    else if (Result == Result.Starting) return LanguageManager.Language["STR_CORE_MSG_LINK_START"];
                    else return LanguageManager.Language["STR_CORE_MSG_LINK_UNKNOWN"];
                case ProgressKind.Attribute:
                    if (Result == Result.Success) return LanguageManager.Language["STR_CORE_MSG_ATTR_OK"];
                    else if (Result == Result.Fail) return LanguageManager.Language["STR_CORE_MSG_ATTR_FAIL"];
                    else if (Result == Result.Starting) return LanguageManager.Language["STR_CORE_MSG_ATTR_START"];
                    else return LanguageManager.Language["STR_CORE_MSG_ATTR_UNKNOWN"];
                case ProgressKind.Delete:
                    if (Result == Result.Success) return LanguageManager.Language["STR_CORE_MSG_DELETE_OK"];
                    else if (Result == Result.Fail) return LanguageManager.Language["STR_CORE_MSG_DELETE_FAIL"];
                    else if (Result == Result.Starting) return LanguageManager.Language["STR_CORE_MSG_DELETE_START"];
                    else return LanguageManager.Language["STR_CORE_MSG_DELETE_UNKNOWN"];
                case ProgressKind.Time_Create:
                    if (Result == Result.Success) return LanguageManager.Language["STR_CORE_MSG_DATE_CREATE_OK"];
                    else return LanguageManager.Language["STR_CORE_MSG_DATE_CREATE_FAIL"];
                case ProgressKind.Time_Access:
                    if (Result == Result.Success) return LanguageManager.Language["STR_CORE_MSG_DATE_ACCTIME_OK"];
                    else return LanguageManager.Language["STR_CORE_MSG_DATE_ACCTIME_FAIL"];
                case ProgressKind.Time_LastWrite:
                    if (Result == Result.Success) return LanguageManager.Language["STR_CORE_MSG_DATE_WRITE_OK"];
                    else return LanguageManager.Language["STR_CORE_MSG_DATE_WRITE_FAIL"];
                default: return null;
            }
        }
    }
}
